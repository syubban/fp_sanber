import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  TextInput,
} from "react-native";
import { FontAwesome } from "@expo/vector-icons";
import { connect } from "react-redux";
import { types, screen } from "../redux/types";
class LoginScreen extends Component {
  constructor() {
    super();
  }

  render() {
    let error = <View></View>;
    if (this.props.isError) {
      error = (
        <Text style={{ color: "red", alignSelf: "center" }}>
          Username atau password salah
        </Text>
      );
    }
    return (
      <View style={styles.container}>
        <FontAwesome name="plus" size={80} color="#ff7979" />
        <Text style={styles.titleText}>Kawal Corona</Text>
        <View style={{ flexDirection: "row" }}>
          <View style={styles.inputGroupBox}>
            <TextInput
              style={styles.inputText}
              placeholder="Username"
              onChangeText={(text) => {
                this.props.setUsername(text);
              }}
            />
            <TextInput
              style={[styles.inputText, { marginVertical: 16 }]}
              placeholder="Password"
              secureTextEntry={true}
              onChangeText={(text) => this.props.setPassword(text)}
            />
            <TouchableOpacity
              style={styles.buttonStyle}
              onPress={() => {
                  console.log(this.props)
                if (
                  this.props.username === "username" &&
                  this.props.password === "password"
                ) {
                  this.props.setError(false);
                  this.props.navigation.reset({
                    index: 0,
                    routes: [{name: 'Home'}],
                  });
                } else {
                  this.props.setError(true);
                }
              }}
            >
              <Text style={{ color: "white", fontWeight: "bold" }}>Log In</Text>
            </TouchableOpacity>
            {error}
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "white",
    justifyContent: "center",
    padding: 24,
  },
  titleText: {
    color: "#ff7979",
    fontSize: 24,
    fontWeight: "bold",
  },
  inputGroupBox: {
    padding: 10,
    flex: 1,
    backgroundColor: "white",
    borderRadius: 4,
  },
  inputText: {
    borderWidth: 1,
    borderRadius: 25,
    paddingHorizontal: 8,
    paddingVertical: 4,
    borderColor: "#95afc0",
  },
  buttonStyle: {
    flex: 1,
    padding: 18,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ff7979",
    borderRadius: 25,
  },
});

const mapStateToProps = (state) => {
  return {
    username: state.login.username,
    password: state.login.password,
    isError: state.login.isError,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setUsername: (username) => {
      dispatch({ type: types.INPUT_USERNAME, screen: screen.LOGIN, username });
    },
    setPassword: (password) => {
      dispatch({ type: types.INPUT_PASSWORD, screen: screen.LOGIN, password });
    },
    setError: (isError) =>
      dispatch({ type: types.ERROR, screen: screen.LOGIN, isError }),
  };
};

LoginScreen = connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
export default LoginScreen;

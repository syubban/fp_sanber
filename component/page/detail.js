import React, { Component, useState } from "react";
import axios from "axios";
import {
  View,
  StyleSheet,
  Text,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
} from "react-native";
import { connect } from "react-redux";
import { types, screen } from "../redux/types";
import { Entypo, MaterialIcons } from "@expo/vector-icons";

class ListItem extends Component {
  state = {
    show: false,
  };
  constructor() {
    super();
  }
  render() {
    const { item } = this.props;
    return (
      <View style={styles.itemSection}>
        <TouchableOpacity
          onPress={() => {
            let show = !this.state.show;
            this.setState({ show });
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <Text style={[styles.itemText, styles.itemTitle]}>
              {item.attributes.Provinsi}
            </Text>
            <View style={{ flexDirection: "column", flex: 1 }}>
              <Entypo
                name={this.state.show ? "chevron-up" : "chevron-down"}
                color={"#fff"}
                size={24}
                style={{ alignSelf: "flex-end" }}
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: "row",
              display: this.state.show ? "flex" : "none",
            }}
          >
            <View style={{ flexDirection: "column" }}>
              <Text style={[styles.itemText, { marginHorizontal: 32 }]}>
                Positif
              </Text>
              <Text style={[styles.itemText, { marginHorizontal: 32 }]}>
                Sembuh
              </Text>
              <Text style={[styles.itemText, { marginHorizontal: 32 }]}>
                Meninggal
              </Text>
            </View>
            <View style={{ flexDirection: "column" }}>
              <Text style={[styles.itemText]}>
                {item.attributes.Kasus_Posi}
              </Text>
              <Text style={[styles.itemText]}>
                {item.attributes.Kasus_Semb}
              </Text>
              <Text style={[styles.itemText]}>
                {item.attributes.Kasus_Meni}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

class DetailScreen extends Component {
  constructor() {
    super();
  }
  componentWillMount() {
    this.getProvinsi();
  }
  getProvinsi = () => {
    this.props.setLoading(true);
    this.props.setError(false);
    axios
      .get("https://api.kawalcorona.com/indonesia/provinsi/", {
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "POST, GET, OPTIONS, PUT, DELETE",
          "Access-Control-Allow-Headers":
            "Origin, Content-Type, Accept, Authorization, X-Request-With",
        },
      })
      .then((data) => {
        this.props.setData(data.data);
      })
      .catch((e) => {
        this.props.setLoading(false);
        this.props.setError(true);
      });
  };
  render() {
    let provinsi = <Text>Tes</Text>;
    if (this.props.isLoading) {
      provinsi = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" />
        </View>
      );
    } else {
      if (!this.props.isError) {
        provinsi = (
          <FlatList
            data={this.props.data}
            renderItem={(data) => <ListItem {...data} />}
            keyExtractor={(item) => item.attributes.FID.toString()}
          />
        );
      } else {
        provinsi = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <MaterialIcons name="error-outline" size={64} color="#bdc3c7" />
            <Text style={{ fontSize: 24, color: "#bdc3c7" }}>
              Oopsie, ada error :({" "}
            </Text>
          </View>
        );
      }
    }
    return (
      <View style={styles.container}>
        <Text style={styles.titleText}>Data Per Provinsi</Text>
        {provinsi}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    padding: 8,
  },
  titleText: {
    fontWeight: "bold",
    fontSize: 20,
    alignSelf: "center",
  },
  itemSection: {
    padding: 16,
    backgroundColor: "#3498DB",
    marginTop: 16,
    marginHorizontal: 16,
    borderRadius: 8,
  },
  itemText: {
    color: "#fff",
  },
  itemTitle: {
    padding: 8,
    paddingTop: 0,
    fontWeight: "bold",
    fontSize: 16,
  },
});

const mapStateToProps = (state) => {
  return {
    data: state.detail.data,
    isLoading: state.detail.isLoading,
    isError: state.detail.isError,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setData: (data) =>
      dispatch({ type: types.UPDATE_DATA, screen: screen.DETAIL, data }),
    setLoading: (isLoading) =>
      dispatch({ type: types.LOADING, screen: screen.DETAIL, isLoading }),
    setError: (isError) =>
      dispatch({ type: types.ERROR, screen: screen.DETAIL, isError }),
  };
};

DetailScreen = connect(mapStateToProps, mapDispatchToProps)(DetailScreen);
export default DetailScreen;

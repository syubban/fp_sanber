import React from 'react';
import { View, Text, StyleSheet, Dimensions, Image, ScrollView } from 'react-native';

export default class About extends React.Component {
    render(){
        return(
            <View style={styles.main_container}>
                <ScrollView>
                    <Text style={{
                        fontSize: 20,
                        alignSelf:'center',
                        fontWeight: '700'
                    }}>Meet the Team</Text>
                    <View style={styles.center_card}>
                        <Image 
                            source={require('../../assets/img_avatar.png')}
                            style={styles.avatar}
                        />
                        <Text style={{
                            fontWeight: 'bold',
                            fontSize: 20
                        }}>Syubban Fakhriya</Text>
                        <Text style={{
                            marginBottom: 5,
                            marginTop: 10,
                            fontWeight: 'bold',
                            fontSize: 15
                        }}>Developer & UI/UX Designer</Text>
                        <Text style={{
                            textAlign: 'justify',
                            fontSize: 15
                        }}>Lorem ipsum dolor sit amet, 
                        consectetur adipiscing elit. 
                        Maecenas scelerisque rhoncus urna. 
                        Duis dictum malesuada lacus, nec dapibus elit sagittis vitae. 
                        Aenean id massa id lacus gravida accumsan. 
                        Mauris eu purus non velit efficitur mattis sit amet ut mauris.</Text>
                    </View>
                    <View style={styles.center_card}>
                        <Image 
                            source={require('../../assets/img_avatar.png')}
                            style={styles.avatar}
                        />
                        <Text style={{
                            fontWeight: 'bold',
                            fontSize: 20
                        }}>Yoga Cahya Romadhon</Text>
                        <Text style={{
                            marginBottom: 5,
                            marginTop: 10,
                            fontWeight: 'bold',
                            fontSize: 15
                        }}>Developer & UI/UX Designer</Text>
                        <Text style={{
                            textAlign: 'justify',
                            fontSize: 15
                        }}>Lorem ipsum dolor sit amet, 
                        consectetur adipiscing elit. 
                        Maecenas scelerisque rhoncus urna. 
                        Duis dictum malesuada lacus, nec dapibus elit sagittis vitae. 
                        Aenean id massa id lacus gravida accumsan. 
                        Mauris eu purus non velit efficitur mattis sit amet ut mauris.</Text>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    main_container: {
        display: 'flex',
        alignItems: 'center',
        padding:8
    },
    center_card: {
        backgroundColor: 'white',
        elevation: 1,
        padding: 20,
        borderRadius: 5,
        borderStyle: 'solid',
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10
    },
    avatar: {
        width: '100%',
        height: 250,
        marginBottom: 20
    }
});